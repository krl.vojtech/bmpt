/*
  ******************************************************************************
  * @file    main.c
  * @author  Vojtech Kral & Krejcir Dominik, Brno University of Technology
  * @version V1.0
  * @date    Nov 30 , 2018
  * @brief   Template for Analog to Digital Converter.
  * @note    measuring of temp, current and voltage, showing it on lcd display
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "settings.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h> 
#include "lcd.h"
#include <math.h>
#include <util/delay.h>

/* State machine  ------------------------------------------------------*/
typedef enum
{
Teplota,
Proud,
Napeti
} type_state;
type_state  current_state = Teplota;

/* Constants and macros ------------------------------------------------------*/
uint16_t kon = 48.8;

/* Function prototypes -------------------------------------------------------*/
void setup(void);

/* Global variables ----------------------------------------------------------*/
 int proud_value =  0;
 int napeti_value = 0;
 int vykon_value =  0;
 
/* User-defined symbols for LCD */
uint8_t lcd_user_symbols[8*2] =
    {
    0b11110, //Degree
    0b10010,
    0b10010,
    0b11110,
    0b00000,
    0b00000,
    0b00000,
    0b00000,

    0b00000, //Micro
    0b00000,
    0b01001,
    0b01001,
    0b01001,
    0b01111,
    0b01000,
    0b01000,
    };

/* Functions -----------------------------------------------------------------*/
// @brief Main function.

int main(void)
{
    /* Initializations */
    setup();

    /* Enables interrupts by setting the global interrupt mask */
    sei();

    /* Forever loop */
    while (1)
    {
        /* start a new conversion on channel  */
        ADCSRA |= (1<<ADSC);

        /* wait for conversion to complete */
        while ((ADCSRA & (1<<ADSC)) != 0)
        ;
        
        char str[2];
        uint16_t value =0;
        uint16_t show =0;
        value = ADC;

        //Choosing measuring
        switch (current_state)
       {
           case Teplota:
           {
           show = (((value * kon) -5000)/ 100); //Calculating Temp
           lcd_gotoxy(10, 1);
           lcd_puts("    ");
           lcd_gotoxy(10, 1);
           itoa(show, str, 10);                 //Conversion from Int to Dec
           lcd_puts(str);                       //Show on LCD
           current_state = Proud;            
           ADMUX &= ~_BV(MUX0) &~_BV(MUX1) &~_BV(MUX2);
           ADMUX  |= _BV (MUX0) |_BV(MUX2);
           _delay_us(100);
           break;
           }
           case Proud:
           {
           lcd_gotoxy(10, 0);
           lcd_puts("    ");
           show = (10*(value*kon))/10000; //Calculation current
           itoa(show, str, 10);
           lcd_gotoxy(10, 0);
           lcd_puts(str);
           ADMUX &= ~_BV(MUX0) &~_BV(MUX1) &~_BV(MUX2);
           ADMUX  |= _BV (MUX0);
           proud_value = show;
           current_state = Napeti;
           _delay_us(100);
           break;
           }
           case Napeti:
           {
           show = (value * kon);    //Calculation voltage
           itoa(show, str, 10);
           lcd_gotoxy(2, 0);
           lcd_puts("    ");
           lcd_gotoxy(2, 0);
           lcd_puts(str); 
           napeti_value = show;
           show = (napeti_value/1000) * proud_value; // Calculation Power
           itoa(show, str, 10);
           lcd_gotoxy(2, 1);
           lcd_puts(str);
           current_state = Teplota;
           ADMUX &= ~_BV(MUX0) &~_BV(MUX1) &~_BV(MUX2);
           ADMUX  |= _BV (MUX0) | _BV (MUX1);
           _delay_us(100);
           break;
           }
       }
       _delay_ms(700);      //delay for better show results
    }

    return 0;
}
/**
  * @brief Setup all peripherals.
  */
void setup(void)
{
    uint8_t i;

    /* Initialize display and select type of cursor */
    lcd_init(LCD_DISP_ON);

    /* Set pointer to beginning of CG RAM memory */
    lcd_command(1<<LCD_CGRAM);

    /* Store two new characters, i.e. 8x1 bytes */
    for (i = 0; i < (8*2); i++)
        {
        lcd_data(lcd_user_symbols[i]);
        }
        
    /* Clear display and set cursor to home position */
    lcd_clrscr();

    /* Set lcd display */
    lcd_gotoxy(0, 0);
    lcd_puts("U= X  V I= Y  mA");
    lcd_gotoxy(0, 1);
    lcd_puts("P= Z mW t= K   C");
    lcd_gotoxy(14, 1);
    lcd_putc(0x00);

    /* Analog to Digital Converter */
    /* register ADMUX: Set ADC voltage reference to AVcc with external capacitor,
                       select input channel ADC3 (PC3) */
    ADMUX|= _BV (REFS0) | _BV (MUX0) | _BV (MUX1);

    /* register ADCSRA: ADC Enable,
                        ADC Prescaler 128 => fadc = fcpu / 128 = 125 kHz */
    ADCSRA |= _BV (ADEN)  |_BV (ADSC) | _BV (ADPS2) | _BV (ADPS1) | _BV (ADPS0);
//| _BV (ADIE)
}


/* END OF FILE ****************************************************************/